﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class StreamForCreationDto
    { 
        [Required]
        public string title { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public string userId { get; set; }
    }
}
