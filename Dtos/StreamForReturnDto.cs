﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class StreamForReturnDto
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string userId { get; set; }
    }
}
