﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Data
{
    public class StreamRepository : IStreamRepository
    {
        private readonly DataContext _context;

        public StreamRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.Remove(_context.Streams.Single(s => s.Id == id));
            _context.SaveChanges();
        }

        public async Task<Streamy> GetLastStream()
        {
            return await _context.Streams.LastOrDefaultAsync(); 
        }

        public async Task<Streamy> GetStream(int id)
        {
            return await _context.Streams.SingleOrDefaultAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<Streamy>> GetStreams()
        {
            return await _context.Streams.ToListAsync();
        }
      
        public async Task<Streamy> Update(IMapper mapper,int id,StreamForEditDto streamForEditDto)
        {
            var streamInDb = await _context.Streams.SingleOrDefaultAsync(s => s.Id == id);

            mapper.Map<StreamForEditDto, Streamy>(streamForEditDto, streamInDb);

            _context.SaveChanges();

            return streamInDb;
        }
    }
}
