﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Data
{
    public interface IStreamRepository
    {
        void Add<T>(T entity) where T : class;
        Task<Streamy> Update(IMapper mapper,int id, StreamForEditDto streamForEditDto);
        void Delete(int id);
        Task<IEnumerable<Streamy>> GetStreams();
        Task<Streamy> GetStream(int id);
        Task<Streamy> GetLastStream();
       
    }
}
