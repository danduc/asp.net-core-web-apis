﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamsController : ControllerBase
    {
        private readonly IStreamRepository _repo;
        private readonly IMapper _mapper;

        public StreamsController(IStreamRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/Streams
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var streams = await _repo.GetStreams();
            var streamsToReturn = _mapper.Map<IEnumerable<StreamForReturnDto>>(streams);

            return Ok(streamsToReturn);
        }

        // GET: api/Streams/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var stream = await _repo.GetStream(id);
          
            return Ok(_mapper.Map<StreamForReturnDto>(stream));
        }

        // POST: api/Streams
        [HttpPost]
        public async Task<IActionResult> Post(StreamForCreationDto streamForCreationDto)
        {
            var stream = _mapper.Map<StreamForCreationDto, Streamy>(streamForCreationDto);

            _repo.Add(stream);

            var lastStream = await _repo.GetLastStream();

            var lastReturnStream = _mapper.Map<Streamy, StreamForReturnDto>(lastStream);

            return StatusCode(210, lastReturnStream);
        }

        // PUT: api/Streams/5
        [HttpPut("{id}")]
        public async Task<Streamy> Put(int id, StreamForEditDto streamForEditDto)
        {
            return await _repo.Update(_mapper, id, streamForEditDto);
        }

        // DELETE: api/Stream/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
